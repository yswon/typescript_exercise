abstract class Department1 {
    protected employees: string[] = [];
    protected constructor(protected readonly id: string, private name: string) {
    }

    // 정적 메서드
    static createEmployee(name: string) {
        return {name: name};
    }

    abstract describe(this: Department1): void;

    addEmployee(employee: string) {
        this.employees.push(employee);
    }

    printEmployeeInformation() {
        console.log(this.employees.length);
        console.log(this.employees);
    }
}

class ITDepartment1 extends Department1{
    constructor(id: string, public admins: string[]) {
        super(id, 'IT');
        this.admins = admins;
    }

    describe() {
        console.log(`id: ${this.id}`);
    }
}

// 싱글톤 패턴
class AccountingDepartment1 extends Department1 {
    private static instance: AccountingDepartment1;
    private constructor(id: string, private reports: string[]) {
        super(id, 'Account');
    }

    static getInstance() {
        if (AccountingDepartment1.instance) {
            return this.instance;
        }
        this.instance = new AccountingDepartment1('d2', []);
        return this.instance;
    }

    describe() {
        console.log(`id: ${this.id}`);
    }
}


//const accounting1 = new AccountingDepartment1('d2', []);
const accounting1 = AccountingDepartment1.getInstance();
const accounting2 = AccountingDepartment1.getInstance();