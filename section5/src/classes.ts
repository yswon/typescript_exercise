class Department {
    //private readonly id: string;
    //private name: string;
    protected employees: string[] = [];

    /*constructor(id: string, n: string) {
        this.id = id;
        this.name = n;
    }*/

    constructor(protected readonly id: string, private name: string) {
    }

    // 정적 메서드
    static createEmployee(name: string) {
        return {name: name};
    }

    describe(this: Department) {
        console.log(`Department: ${this.id}, ${this.name}`);
    }

    addEmployee(employee: string) {
        this.employees.push(employee);
    }

    printEmployeeInformation() {
        console.log(this.employees.length);
        console.log(this.employees);
    }
}

const accounting = new Department('d1', 'Accounting');
accounting.addEmployee('Max');
accounting.addEmployee('Kim');
//accounting.employees[2] = 'Anna';

accounting.describe();
accounting.printEmployeeInformation();

/*
const accountingCopy = { name: 'DUMMY', describe: accounting.describe};
accountingCopy.describe();*/

class ITDepartment extends Department{
    constructor(id: string, public admins: string[]) {
        super(id, 'IT');
        this.admins = admins;
    }
}
const itAccounting = new ITDepartment('d1', ['Max']);
console.log(itAccounting);
itAccounting.describe();
itAccounting.printEmployeeInformation();


class AccountingDepartment extends Department {
    private lastReport: string;
    get mostRecentReport() {
        if (this.lastReport) {
            return this.lastReport;
        }
        throw new Error('No report found.')
    }

    set mostRecentReport(value: string) {
        if (!value) {
            throw new Error('Please pass in a valid value!');
        }
        this.addReport(value);
    }

    constructor(id: string, private reports: string[]) {
        super(id, 'Account');
        this.lastReport = reports[0];
    }

    // Override
    describe() {
        console.log(`Accounting Department - ID : ${this.id}`);
    }

    // Override
    addEmployee(name: string) {
        if (name === 'Max') {
            return;
        }
        // protected
        this.employees.push(name);
    }

    addReport(text: string) {
        this.reports.push(text);
        this.lastReport = text;
    }

    getReport() {
        console.log(this.reports);
    }
}

const accountingDepartment = new AccountingDepartment('d2', []);
accountingDepartment.addReport('Something went wrong....');
accountingDepartment.getReport();
accountingDepartment.addEmployee('Max');
accountingDepartment.addEmployee('Kim');
accountingDepartment.printEmployeeInformation();

console.log(accountingDepartment.mostRecentReport)

accountingDepartment.mostRecentReport = 'Year End Report';
accountingDepartment.getReport();


// static method
const employee1 = Department.createEmployee('Max');
console.log(employee1);