const userName = 'Max';
let age = 30;
age = 29;

/*function add(a: number, b: number) {
    var result;
    result = a + b;
    return result;
}

console.log(result);*/

let add = (a: number, b:number) => {
    return a + b;
};
add = (a: number, b:number) => a * b;
console.log(add(1,2));

let add2 = (a: number, b:number = 3) => a * b;
console.log(add2(2));


let printOutput = (output: string | number) => console.log(output);
let printOutput2: (output: string | number) => void = output => console.log(output);
printOutput(add(2, 5));
printOutput2(add(2,5));

const button = document.querySelector('button');
console.log(button)
if (button) {
    button.addEventListener('click', event => console.log(event));
}


const hobbies = ['Sports', 'Cooking', 'Swim'];
const activeHobbies = ['Hiking'];

//activeHobbies.push(hobbies[0], hobbies[1]);
activeHobbies.push(...hobbies);
console.log(activeHobbies);

const activeHobbies2 = ['Hiking', ...hobbies];
console.log(activeHobbies2);

const person = {
    firstName: 'Max',
    thisAge: 30
}

const copedPerson = { ...person };
console.log(copedPerson);

const add3 = (...numbers: number[]) => {
    let result = 0;
    return numbers.reduce((curResult,curValue) => {
        return curResult + curValue;
    }, 0)
}

const addedNumbers = add3(1,2,3,4,5.6);
console.log(addedNumbers);

/*const hobby1 = hobbies[0];
const hobby2 = hobbies[1];*/
const [hobby1, hobby2, ...remainingHobbies] = hobbies;
console.log(hobby1, hobby2, remainingHobbies)

const {firstName: thisName, thisAge} = person;
console.log(thisName, thisAge)
