import _ from 'lodash';
import "reflect-metadata";
import { plainToClass } from 'class-transformer';
import { validate } from "class-validator";
import {Product} from "./product.mode";

console.log(_.shuffle([1,2,3]));

declare var GLOBAL: any;

console.log(GLOBAL);


//
const p1 = new Product('A Book', 12.99);
console.log(p1.getInformation());


const products = [
    {title: 'A Carpet', price: 29.99},
    {title: 'A Book', price: 10.99}
];

const loadedProducts = products.map(prod => {
    return new Product(prod.title, prod.price);
})

for (const prod of loadedProducts) {
    console.log(prod.getInformation());
}

const products1 = [
    {title: 'A Carpet', price: 29.99},
    {title: 'A Book', price: 10.99}
];

const loadedProducts1 = plainToClass(Product, products1);
for (const prod of loadedProducts1) {
    console.log(prod.getInformation());
}

//
const newProd = new Product('', -5.4);
validate(newProd).then(errors => {
    if (errors.length > 0) {
        console.log('validation error');
        console.log(errors);
    } else {
        console.log(newProd.getInformation());
    }
});
