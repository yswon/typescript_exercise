/*const person: {
    name: string,
    age: number
} = {*/
const person: {
    name: string;
    age: number;
    bobbies: string[];
    role: [number, string];  // tuple
} = {
    name: 'Maximilian',
    age: 30,
    bobbies: ['Sports', 'Cooking'],
    role: [2, 'author']
}

//person.role.push('test');
//person.role.push('test1');
person.role = [0, 'admin'];
// person.role = [2, 'admin', 'user'];
console.log(person.role);

let favoriteActivities: string[];
favoriteActivities = ['Sports'];

console.log(person.name);

for (const hobby of person.bobbies) {
    console.log(hobby.toUpperCase());
    // console.log(hobby.map());
}