/*const person: {
    name: string,
    age: number
} = {*/
var person = {
    name: 'Maximilian',
    age: 30,
    bobbies: ['Sports', 'Cooking'],
    role: [2, 'author']
};
//person.role.push('test');
//person.role.push('test1');
person.role = [0, 'admin'];
console.log(person.role);
// person.role = [2, 'admin', 'user'];
var favoriteActivities;
favoriteActivities = ['Sports'];
console.log(person.name);
for (var _i = 0, _a = person.bobbies; _i < _a.length; _i++) {
    var hobby = _a[_i];
    console.log(hobby.toUpperCase());
    // console.log(hobby.map());
}
