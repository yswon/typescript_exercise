// type alias
type Combinable = number | string;
type ConversionDescriptor =  'as-number' | 'as-text';   // literal type

function combine(
    input1: Combinable,
    input2: Combinable,
    resultConversion: ConversionDescriptor
) {
    let result;
    if (typeof input1 == 'number' && typeof input2 == 'number' || resultConversion == 'as-number') {
        result =  +input1 + +input2;
    } else {
        result = input1.toString() + input2.toString();
    }

    return result.toString();
}

const combineAges = combine(30, 26, 'as-number');
console.log(combineAges);

const combineStringAges = combine('30', '26', 'as-number');
console.log(combineStringAges)

const combineNames = combine('Max', 'Anna', 'as-text');
console.log(combineNames);

//
type User = { name: string; age: number };
const u1: User = { name: 'Max', age: 30 }; // this works!

function greet(user: { name: string; age: number }) {
    console.log('Hi, I am ' + user.name);
}

function greetByType(user: User) {
    console.log('Hi, I am ' + user.name);
}

function isOlder(user: { name: string; age: number }, checkAge: number) {
    return checkAge > user.age;
}

function isOlderByType(user: User, checkAge: number) {
    return checkAge > user.age;
}