function add(n1, n2) {
    return n1 + n2;
}
function printResult(num) {
    console.log('Result : ' + num);
}
function addAndHandle(n1, n2, cb) {
    var result = n1 + n2;
    console.log('re: ' + cb(result));
}
printResult(add(5, 7));
// function type
//let combineValues : Function;
var combineValues;
combineValues = add;
//combineValues = printResult;
//combineValues = 5;
console.log(combineValues(5, 5));
addAndHandle(10, 20, function (result) {
    //console.log(result);
    return result;
});
