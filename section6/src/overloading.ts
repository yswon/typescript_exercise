//
function add1(a: string, b: string): string;
function add1(a: number, b: number): number;
function add1(a: string, b: number): string;
function add1(a: number, b: string): string;
function add1(a: Combinable, b: Combinable) {
    // 타입가드
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }
    return a + b;
}

const result = add1(1, 5);
const result1 = add1('Max', 'Kim');
result1.split('');

