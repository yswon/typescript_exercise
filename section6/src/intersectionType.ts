// intersection type
type Admin = {
    name: string;
    privileges: string[];
}

type Employee = {
    name: string;
    startDate: Date;
}

type ElevatedEmployee = Admin & Employee;

const e1: ElevatedEmployee = {
    name: 'Max',
    privileges: ['create-server'],
    startDate: new Date()
}

// interface도 가능
interface Admin1 {
    name: string;
    privileges: string[];
}

interface Employee1 {
    name: string;
    startDate: Date;
}

interface ElevatedEmployee1 extends Admin1, Employee1 {}

const e2: ElevatedEmployee1 = {
    name: 'Max',
    privileges: ['create-server'],
    startDate: new Date()
}