//import axios from "axios";

const form = document.querySelector('form')!;
/*const addressInput = document.getElementById('address')! as HTMLInputElement;
const GOOGLE_API_KEY = ''*/

declare var ol: any;

/*type GoogleGeocodingResponse = {
    results: {geometry: {location: {lat: number, lng: number}}}[];
    status: 'OK' | 'ZERO RESULTS';
};*/

function searchAddressHandler(event: Event) {
    event.preventDefault();
    /*
    const enteredAddress = addressInput.value;

    axios.get<GoogleGeocodingResponse>(
        `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURI(enteredAddress)}&key=${GOOGLE_API_KEY}`
    )
    .then(response => {
        console.log(response);
        if (response.data.status !== 'OK') {
            throw new Error('Could not fetch location!!');
        } else {
            const coorinates = response.data.results[0].geometry.location;

        }
    })
    .catch(err => {
        alert(err.mesage);
        console.log(err)
    });*/

    const coordinates = {lat: 40.41, lng: -73.99}; // Google API에서 좌표를 가져올 수 없음, 더미 API 사용
    document.getElementById('map')!.innerHTML = ''; // <div id="map">에서 <p> 제거
    new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            })
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([coordinates.lng, coordinates.lat]),
            zoom: 16
        })
    });
}

form.addEventListener('submit', searchAddressHandler);