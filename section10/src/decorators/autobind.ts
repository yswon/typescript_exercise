
// autoBind decorator
export function autoBind(target: any, methodName: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;
    const adjDescriptor: PropertyDescriptor = {
        configurable: true,
        get() {
            return  originalMethod.bind(this);
        }
    };
    return adjDescriptor;
}
