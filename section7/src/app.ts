// 1: 빌트인 제네릭
const names: Array<string> = ['max']; // string[]
names[0].split('');

const promise: Promise<string> = new Promise((resolve => {
    setTimeout(()=> {
        resolve('text');
    })
}));

promise.then(data => {
    data.split('');
})

// 2 일반 함수
function merge(objA: object, objB: object) {
    return Object.assign(objA, objB);
}

console.log(merge({name: 'Max'}, {age: 30}));

const mergeObj = merge({name: 'Max'}, {age: 30});
//console.log(mergeObj.name);

function merge1<T, U>(objA: T, objB: U) {
    return Object.assign(objA, objB);
}

const mergeObj1 = merge1({name: 'Max'}, {age: 30});
console.log(mergeObj1.name);

const mergeObj2 = merge1<{name: string}, {age: number}>({name: 'Max'}, {age: 30});

// 3. 제약 조건
const mergeObj3 = merge1({name: 'Max'}, 30);
console.log(mergeObj3); // 30?

function merge2<T extends object, U extends object>(objA: T, objB: U) {
    return Object.assign(objA, objB);
}

//const mergeObj4 = merge2({name: 'Max'}, 30);

// 4. 다른 일반함수
interface Lengthy {
    length: number;
}

function countAndDescribe<T extends Lengthy>(element: T): [T, string] {
    let descriptionText = 'Got no value.';
    if (element.length === 1) {
        descriptionText = 'Got 1 element.';
    } else if(element.length > 1) {
        descriptionText = 'Got ' + element.length + ' elements.';
    }
    return [element, descriptionText];
}

console.log(countAndDescribe('Hi there!'));
console.log(countAndDescribe(['Max', 'Kim']));

// 5. keyof
function extractAndConvert<T extends object, U extends keyof T>(obj: T, key: U) {
    return 'Value : ' + obj[key];
}

extractAndConvert({name: 'Max'}, 'name');

// 6. 제네릭 클래스

class DataStorage<T extends string | number | boolean > {
    private data: T[] = [];

    addItem(item: T) {
        this.data.push(item);
    }

    removeItem(item: T) {
        this.data.splice(this.data.indexOf(item), 1);
    }

    getItems() {
        return [...this.data];
    }
}

const textStorage = new DataStorage<string>();
//textStorage.addItem(10);
textStorage.addItem('Max');
textStorage.addItem('Kim');
textStorage.removeItem('Max');
console.log(textStorage.getItems());

const numberStorage = new DataStorage<number>();
numberStorage.addItem(1);
numberStorage.addItem(2);
console.log(numberStorage.getItems());


/*
const objectStorage = new DataStorage<object>();
objectStorage.addItem({name: 'Max'});
objectStorage.addItem({name: 'Kim'});
objectStorage.removeItem({name: 'Max'});
console.log(objectStorage.getItems());  // ??
 */

// 7. 일반 유틸리티 타입
// Partial
interface CourseGoal {
    title: string;
    description: string;
    completeUtil: Date;
}

function createCourseGoal(title: string, description: string, date: Date) {
    let courseGoal: Partial<CourseGoal> = {};
    courseGoal.title = title;
    courseGoal.description = description;
    courseGoal.completeUtil = date;
    return courseGoal;
}

// Readonly
const name1: Readonly<string[]> = ['Max', 'Kim'];
//name1.push('Anna');
//name1.pop();